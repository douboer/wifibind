
# WIFI 绑定脚本

## 说明

## 用法
wifibind.py [-h] [-i IFILES] [-c] [-C]

optional arguments
  -h, --help  帮助信息
  -i IFILES   ifiles文件或文件列表，用逗号分割，包含一些特定字符可能需要用单引号，如：'1().xlsx,2.xlsx'
  -c或-C      开关，表示用匹配表头的方式解析输入表

## 问题
1. 拼接作用&规则？
输出转换后的MAC拼接字符串到标准输出和joint.txt
2. SSID是否固定是酒店缩写+房间号
是
3. 输入表检查什么？
目前检查房间号、房间号缩写、MAC、电话格式
4. MAC前六位对应多个型号，如何选择？
自动检查用户提供表中的model信息，提取型号

## 类与函数说明

### class WifiBind

### gcheck_cn(self, istr)
检查字符串包含中文.

### gparse_customer_table(self, ifn)
解析客户表(输入表)，固定格式或正则匹配表头.
正则如下
```
posdict = {'roomnum':{'pos':-1,'key':'房间号'},
        'itvaccount':{'pos':-1,'key':'ITV'},
        'bbaccount':{'pos':-1,'key':'^宽带'},
        'mac':{'pos':-1,'key':'MAC'},
        'contact':{'pos':-1,'key':'联系人'},
        'phone':{'pos':-1,'key':'^联系电话'},
        'hotelname':{'pos':-1,'key':'酒店.*名称'},
        'hoteladdr':{'pos':-1,'key':'酒店地址'},
        'abbr':{'pos':-1,'key':'酒店缩写'},
        'province':{'pos':-1,'key':'(浙江){0,1}省(（必填）){0,1}$'},
        'city':{'pos':-1,'key':'^市'},
        'district':{'pos':-1,'key':'^区'},
        'model':{'pos':-1,'key':'终端型号'}}
```

### gprocess_output_table(self, ifn, ofn)
生成输出表，表头"设备mac","宽带账号","商户账号(手机号)","商户名称","一级行业","二级行业","联系人","联系方式","商户地址","设备SSID(例:yx-123)","省(例:浙江)","市(例:杭州)","区(例:西湖区)","经度","纬度","商户名称缩写","房间号","错误信息"

### gget_model_idx(self,md)
convenient to find offset from MODELS dict

### gparse_mac_model_table(self, mpt)
discard XXXXXXXXX,
substitude by json file which obtained from all device tables

### gparse_devices_table(self, mpt)
parse all device tables in order to obtain the relationship between mac prefix and model

### gtrans_awifi_mac(self, mmodelidx, mmac)
输入MAC地址和型号序号，转换MAC地址

### gtrans_awifi_mac2(self, mmac)
输入MAC地址转换MAC地址
Python多态?

### MAC长度不足，补零规则？
???

### glist_files(self, mpt)
input directory, return all files included

### gdict2json(self,d)
字典转json格式

### gjson2dict(self,jf)
json格式转字典

### gget_index_model_dict(self)
获取序号-型号-MAC前六位关系字典
{"9": { "factory": "NSB", "offset": -7, "model": "RG020ET-CA", "pre_mac_list": [ "6CEFC6", ...] },

### gget_model_macs_dict(self)
获取型号与MAC前六位关系的字
{ "B860GV1.1": { "factory": "ZTE", "idx": 5, "mac": 2, "pre_mac_list": [ "049573",...]}}

### gparse_files_list(self, argv)
解析命令行输入参数

