#########################################################
## @file   : txls.py
## @desc   :
## @create : 2021/08/06
## @author : Chengan
## @email  : douboer@gmail.com
#########################################################

import re
import sys
import os
import io
import json
import openpyxl  # excel manipulation

from collections import defaultdict

if __name__=='__main__':

    sheetdict = defaultdict(dict)

    wb= openpyxl.load_workbook('testxls.xlsx')
    sheetnames = wb.sheetnames
    ws = wb[sheetnames[0]]

    print('Test xls num lines {} num columns {}'.format(ws.max_row, ws.max_column))

    for row in ws.iter_rows():
        for cell in row:
            print(cell.coordinate, cell.value)

    pass
